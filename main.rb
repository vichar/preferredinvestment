#!/usr/bin/ruby
require 'sinatra'
require 'json'
require_relative "Facades/customer_porfolios"

get '/totalPortfolioValues' do
  portfolios = CustomerPorfolios.new
  results = portfolios.getTotalPortfolioValues
  content_type :json
  results.to_json
end
get '/totalPortfolioValues/:id' do
  portfolios = CustomerPorfolios.new
  result = portfolios.getTotalPortfolioValues.find{|portfolio| portfolio[:customerID]  == params['id']}
  content_type :json
  result.to_json
end

get '/segmentHoldingValues' do
  portfolios = CustomerPorfolios.new
  results = portfolios.getHoldingSegmentValues
  content_type :json
  results.to_json
end

get '/segmentHoldingValues/:id' do
  portfolios = CustomerPorfolios.new
  result = portfolios.getHoldingSegmentValues.find{|portfolio| portfolio[:customerID]  == params['id']}
  content_type :json
  result.to_json
end

get '/preferredInvestmentSectors' do
  portfolios = CustomerPorfolios.new
  content_type :json
  portfolios.getPreferredInvestmentSector.to_json
end

get '/preferredInvestmentSectors/:id' do
  portfolios = CustomerPorfolios.new
  results = portfolios.getPreferredInvestmentSector.find{|portfolio| portfolio[:customerID]  == params['id']}
  content_type :json
  results.to_json
end

get '/getInvestmentValues' do
  portfolios = CustomerPorfolios.new
  results = portfolios.getHoldingSegmentValues
  content_type :json
  results.to_json
end

get '/getInvestmentValues/:id' do
  portfolios = CustomerPorfolios.new
  results = portfolios.getHoldingSegmentValues.find{|portfolio| portfolio[:customerID]  == params['id']}
  content_type :json
  results.to_json
end

get '/portfolios/:id' do
  portfolios = CustomerPorfolios.new
  results = portfolios.getAllTransactions.find{|portfolio| portfolio[:customerID]  == params['id']}
  content_type :json
  results.to_json
end



get '/portfolios' do
  portfolios = CustomerPorfolios.new
  results = portfolios.getAllTransactions
  content_type :json
  results.to_json
end

post '/endpoint' do 
  %q{
    <h1>Success!</h1>
  }
end

get '/' do
  %q{
    <h1>Hello World!</h1>
  }
end
