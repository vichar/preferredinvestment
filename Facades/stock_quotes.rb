#!/usr/bin/ruby
require 'csv'
require_relative "struct_initializer"
class StockQuotes
  $tickersPath = './Facades/data/tickers.csv'
  $tickers = Hash.new
  $tickerHeaders = ['ticker','segment','cost']

  def initialize
      $tickers = prepareData($tickersPath)
  end

  def getStock(stockId)
    $tickers.find{|ticker| ticker['ticker'] == stockId}
  end

  private
  def prepareData(filePath)
    $tickers = StructIntializer.parse(filePath,$tickerHeaders)
  end

end
