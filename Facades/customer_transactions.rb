#!/usr/bin/ruby
require_relative "struct_initializer"

class CustomerTransactions
  $transactionsHeaders = ['customerID','ticker','unit']
  $transactionsPath = './Facades/data/transactions.csv'
  $transactions= Array.new
  $portfolios = Array.new

  def initialize
      $transactions = prepareData($transactionsPath)
  end

  def getAllTransactions
    $transactions
  end


  private
  def prepareData(filePath)
    $transactions = StructIntializer.parse($transactionsPath,$transactionsHeaders)
  end

end
