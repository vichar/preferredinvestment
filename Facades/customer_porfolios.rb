#!/usr/bin/ruby
require_relative 'struct_initializer'
require_relative 'stock_quotes'
require_relative 'customer_transactions'

class CustomerPorfolios

  $portfolios = Array.new
  $transactions = Array.new
  $stockQuotes = StockQuotes.new
  def initialize
      $transactions = prepareData($transactionsPath)
  end

  def getAllTransactions
    $portfolios
  end

  def getPreferredInvestmentSector
    customerPorfolios = self.getHoldingSegmentValues
    customerPorfolios.map!{
      |customerPorfolio|
          Hash(:customerID => customerPorfolio[:customerID], :investments =>
            customerPorfolio[:investments].sort_by { |investment| investment[:value] }.reverse
          )
    }.map!{
      |customerPorfolio|
          Hash(:customerID => customerPorfolio[:customerID], :preferredInvestement =>
            Hash(:segment => customerPorfolio[:investments].first[:name], :value => customerPorfolio[:investments].first[:value])
          )
    }
  end
  def getTotalPortfolioValues
    customerPorfolios = self.getHoldingSegmentValues
    customerPorfolios.map!{
      |customerPorfolio|
          Hash(:customerID => customerPorfolio[:customerID], :values =>
            customerPorfolio[:investments].inject(0) { |value,investment| value + investment[:value] }
          )
    }
  end

  def getHoldingSegmentValues
   costedPortfolios = $portfolios.each{
      |portfolio|
            portfolio[:assets].each{
              |asset|
                quote = $stockQuotes.getStock(asset[:ticker])
                asset[:segment] = quote['segment']
                asset[:value] = quote['cost'].to_f * asset[:unit].to_f
            }
    }
    costedPortfolios.map!{ |portfolio|
          Hash(:customerID => portfolio[:customerID], :investments => portfolio[:assets].group_by{|asset| asset[:segment]}.each{|_, value| value.map!{|asset| asset[:value].to_f}})
    }.map!{
      |costedPortfolio|
          Hash(:customerID => costedPortfolio[:customerID], :investments =>
            costedPortfolio[:investments].map{|segmentName,value|
              {:name => segmentName, :value => value.inject(0,:+)}
           }
          )
    }

  end
  private
  def prepareData(filePath)
    customerTransactions = CustomerTransactions.new
    $transactions = customerTransactions.getAllTransactions
    initializeCustomerPorfolios
    populateCustomerPorfolios
  end
  def initializeCustomerPorfolios
    $portfolios = $transactions.uniq { |transaction| transaction['customerID']} .map { |transaction| {:customerID => transaction['customerID'], :assets => Array.new}}
    $transactions.each{ |transaction|
      $portfolios.find_index {
        |customer|
        customer[:customerID] == transaction['customerID']
      }
    }

  end

  def populateCustomerPorfolios
    $portfolios.each{
      |customer|
      $transactions.each{
       |transaction|
        if transaction['customerID'] == customer[:customerID]
          customer[:assets].push Hash( :ticker => transaction['ticker'], :unit => transaction['unit'] )
        end
      }
    }
  end

end
