class StructIntializer

  def self.parse(filePath, headers)
    items = Array.new
    CSV.foreach(filePath, headers: true) do |row|
        items.push(self.initializeObject(headers,row))
    end
    items
  end

  def self.initializeObject(headers, rawData)
    item = Hash.new
    headers.each{|header| item[header] = rawData[header]}
    item
  end

end
