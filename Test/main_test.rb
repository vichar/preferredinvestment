require_relative "../main"
require 'test/unit'
require 'rack/test'
require 'json'

class MainTest < Test::Unit::TestCase

  def test_it_says_hello_world
     browser = Rack::Test::Session.new(Rack::MockSession.new(Sinatra::Application))
     browser.get '/'
     assert browser.last_response.ok?
     assert_match(/Hello World!/, browser.last_response.body)
  end

  def test_portfolios_end_point
    browser = Rack::Test::Session.new(Rack::MockSession.new(Sinatra::Application))
    browser.get '/portfolios'
    assert browser.last_response.ok?
    response = JSON.parse browser.last_response.body
    assert response.length >= 1
    portfolio = response.first
    assert portfolio.has_key? 'customerID'
    assert portfolio.has_key? 'assets'
    assert portfolio['assets'].length >= 1
    asset = portfolio['assets'].first
    assert asset.has_key? 'ticker'
    assert asset.has_key? 'unit'
  end
  def test_portfolios_id_end_point
    browser = Rack::Test::Session.new(Rack::MockSession.new(Sinatra::Application))
    browser.get '/portfolios/7260780001'
    assert browser.last_response.ok?
    response = JSON.parse browser.last_response.body
    portfolio = response
    assert portfolio.has_key? 'customerID'
    assert portfolio.has_key? 'assets'
    asset = portfolio['assets'].first
    assert asset.has_key? 'ticker'
    assert asset.has_key? 'unit'
  end

  def test_getInvestmentValues_end_point
    browser = Rack::Test::Session.new(Rack::MockSession.new(Sinatra::Application))
    browser.get '/getInvestmentValues'
    assert browser.last_response.ok?
    response = JSON.parse browser.last_response.body
    assert response.length >= 1
    portfolio = response.first
    assert portfolio.has_key? 'customerID'
    assert portfolio.has_key? 'investments'
    asset = portfolio['investments'].first
    assert asset.has_key? 'name'
    assert asset.has_key? 'value'
  end

  def test_getInvestmentValues_id_end_point
    browser = Rack::Test::Session.new(Rack::MockSession.new(Sinatra::Application))
    browser.get '/getInvestmentValues/7260780001'
    assert browser.last_response.ok?
    response = JSON.parse browser.last_response.body
    portfolio = response
    assert portfolio.has_key? 'customerID'
    assert portfolio.has_key? 'investments'
    asset = portfolio['investments'].first
    assert asset.has_key? 'name'
    assert asset.has_key? 'value'
  end




end
